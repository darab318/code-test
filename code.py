import random

def remove_duplicates(numbers):
    return sorted(set(numbers), reverse=True) # Removes duplicates and sorts list in descending order
    
numbers = random.sample(range(1, 101), 6) # Creates a list of six random numbers ranging from 1 to 100
duplicates = random.sample(numbers, 4) # Four duplicate numbers
numbers += duplicates # Add duplicate numbers to list

sorted_numbers = remove_duplicates(numbers)
print(sorted_numbers)
